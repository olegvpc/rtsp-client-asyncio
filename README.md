# Rtsp Client Asyncio

## Interface of dialog with RTSP protocol  

### for start 
```python
python3 main.py 

```

https://habr.com/ru/post/117735/

```text
01: OPTIONS rtsp://192.168.0.254/jpeg RTSP/1.0
02: CSeq: 1
03: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)
04: 
05: RTSP/1.0 200 OK
06: CSeq: 1
07: Date: Fri, Apr 23 2010 19:54:20 GMT
08: Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE
09: 
10: DESCRIBE rtsp://192.168.0.254/jpeg RTSP/1.0
11: CSeq: 2
12: Accept: application/sdp
13: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)
14: 
15: RTSP/1.0 200 OK
16: CSeq: 2
17: Date: Fri, Apr 23 2010 19:54:20 GMT
18: Content-Base: rtsp://192.168.0.254/jpeg/
19: Content-Type: application/sdp
20: Content-Length: 442
21: x-Accept-Dynamic-Rate: 1
22: 
23: v=0
24: o=- 1272052389382023 1 IN IP4 0.0.0.0
25: s=Session streamed by "nessyMediaServer"
26: i=jpeg
27: t=0 0
28: a=tool:LIVE555 Streaming Media v2008.04.09
29: a=type:broadcast
30: a=control:*
31: a=range:npt=0-
32: a=x-qt-text-nam:Session streamed by "nessyMediaServer"
33: a=x-qt-text-inf:jpeg
34: m=video 0 RTP/AVP 26
35: c=IN IP4 0.0.0.0
36: a=control:track1
37: a=cliprect:0,0,720,1280
38: a=framerate:25.000000
39: m=audio 7878 RTP/AVP 0
40: a=rtpmap:0 PCMU/8000/1
41: a=control:track2
42: 
43: 
44: SETUP rtsp://192.168.0.254/jpeg/track1 RTSP/1.0
45: CSeq: 3
46: Transport: RTP/AVP;unicast;client_port=41760-41761
47: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)
48: 
49: RTSP/1.0 200 OK
50: CSeq: 3
51: Cache-Control: must-revalidate
52: Date: Fri, Apr 23 2010 19:54:20 GMT
53: Transport: RTP/AVP;unicast;destination=192.168.0.4;source=192.168.0.254;client_port=41760-41761;
            server_port=6970-6971
54: Session: 1
55: x-Transport-Options: late-tolerance=1.400000
56: x-Dynamic-Rate: 1
57: 
58: SETUP rtsp://192.168.0.254/jpeg/track2 RTSP/1.0
59: CSeq: 4
60: Transport: RTP/AVP;unicast;client_port=7878-7879
61: Session: 1
62: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)
63: 
64: RTSP/1.0 200 OK
65: CSeq: 4
66: Cache-Control: must-revalidate
67: Date: Fri, Apr 23 2010 19:54:20 GMT
68: Transport: RTP/AVP;unicast;destination=192.168.0.4;source=192.168.0.254;client_port=7878-7879;
            server_port=6972-6973
69: Session: 1
70: x-Transport-Options: late-tolerance=1.400000
71: x-Dynamic-Rate: 1
72: 
73: PLAY rtsp://192.168.0.254/jpeg/ RTSP/1.0
74: CSeq: 5
75: Session: 1
76: Range: npt=0.000-
77: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)
78: 
79: RTSP/1.0 200 OK
80: CSeq: 5
81: Date: Fri, Apr 23 2010 19:54:20 GMT
82: Range: npt=0.000-
83: Session: 1
84: RTP-Info: url=rtsp://192.168.0.254/jpeg/track1;seq=20730;
            rtptime=3869319494,url=rtsp://192.168.0.254/jpeg/track2;seq=33509;rtptime=3066362516
85: 
86: # В этот момент начинается передача контента и следующая команда вызывается для остановки вещания
87: 
88: TEARDOWN rtsp://192.168.0.254/jpeg/ RTSP/1.0
89: CSeq: 6
90: Session: 1
91: User-Agent: VLC media player (LIVE555 Streaming Media v2008.07.24)
92: 
93: RTSP/1.0 200 OK
94: CSeq: 6
95: Date: Fri, Apr 23 2010 19:54:25 GMT

```