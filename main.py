import asyncio
from client import Client
# from storage import Storage
# from shared import Shared


async def main():
    client_rtsp = Client()
    print(client_rtsp.__doc__)
    await client_rtsp.connect()
    await client_rtsp.play()


if __name__ == '__main__':
    asyncio.run(main())
